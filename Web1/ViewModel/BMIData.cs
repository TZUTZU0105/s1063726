﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web1.ViewModel
{
    public class BMIData
    {

        [Display(Name = "體重")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]
        public float? weight { get; set; }


        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(50, 200, ErrorMessage = "請輸入50-200的數值")]
        public float? height { get; set; }


        public float? Result { get; set; }

        public string Level { get; set; } 
    }
}